export type Connect = {
    name: string;
}

export type ChatConnected = {
    name: string;
    userId: number;
    users: { userId: number; name: string; }[];
};

export type UserJoined = {
    name: string;
    userId: number;
}

export type UserLeft = {
    name: string;
    userId: number;
};

export type NewMessage = {
    message: string;
    userId: number;
}