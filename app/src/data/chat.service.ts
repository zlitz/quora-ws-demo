import { WsService } from './ws.service';
import { Observable } from 'rxjs';
import { WsMessage } from './ws-message';
import { map, filter } from 'rxjs/operators';
import {
    ChatConnected,
    Connect,
    NewMessage,
    UserJoined,
    UserLeft
} from './chat-messages';

export class ChatService {

    private readonly _wsService: WsService;

    public constructor(wsService: WsService) {
        this._wsService = wsService;
    }

    public connect(chattersName: string): Observable<ChatConnected> {

        const message: WsMessage<Connect> = {
            data: { name: chattersName },
            id: Math.random(),
            senderId: -1,
            type: 'connect'
        };
        return this._wsService.sendMessageWithResponse<Connect, ChatConnected>(message).pipe(map(data => data.data));
    }

    public chatterConnected(): Observable<UserJoined> {
        return this._wsService.observeMessageReceived().pipe(filter(a => a.type === 'userJoined'), map(a => a.data as UserJoined));
    }

    public chatterDisconnected(): Observable<UserLeft> {
        return this._wsService.observeMessageReceived().pipe(filter(a => a.type === 'userLeft'), map(a => a.data as UserLeft));
    }

    public messageReceived(): Observable<NewMessage> {
        return this._wsService.observeMessageReceived().pipe(filter(a => a.type === 'newMessage'), map(a => a.data as NewMessage));
    }

    public sendMessage(chatMessage: string, id: number): void {
        const message: WsMessage<NewMessage> = {
            data: { userId: id, message: chatMessage },
            id: Math.random(),
            senderId: -1,
            type: 'newMessage'
        };

        this._wsService.sendMessage(message);
    }
}