export type WsMessage<T> = {
    data: T;
    id: number;
    senderId: number;
    type: string;
}