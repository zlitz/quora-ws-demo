
import { BehaviorSubject, Observable, Subscriber, Subject } from 'rxjs';
import { first } from 'rxjs/operators';
import { WsMessage } from './ws-message';

export class WsService {

    private _openSubject = new BehaviorSubject<boolean>(false);
    private _receivedSubject = new Subject<WsMessage<any>>();
    private _wsClosedSubject = new Subject<void>();
    private _ws: WebSocket;

    public constructor(url: string) {

        this._ws = new WebSocket(url);

        this._ws.onopen = () => this._openSubject.next(true);

        this._ws.onclose = () => this._wsClosedSubject.next();

        this._ws.onmessage = event => {
            const message = JSON.parse(event.data);
            console.clear();
            console.log(message);
            this._receivedSubject.next(message);
        }
    }

    public observeClosed(): Observable<void> {
        return this._wsClosedSubject;
    }

    public observeOpen(): Observable<boolean> {
        return this._openSubject;
    }

    public observeMessageReceived(): Observable<WsMessage<any>> {
        return this._receivedSubject;
    }

    public sendMessage(message: WsMessage<any>): void {
        this._openSubject.pipe(first(value => value === true)).subscribe(() => {
            this._ws.send(JSON.stringify(message));
        });
    }

    public sendMessageWithResponse<T, R>(message: WsMessage<T>): Observable<WsMessage<R>> {
        return Observable.create((subscriber: Subscriber<WsMessage<any>>) => {

            this._openSubject.pipe(first(value => value === true)).subscribe(() => {
                this._ws.send(JSON.stringify(message));
            });

            this._receivedSubject.pipe(first(m => m.senderId === message.id)).subscribe(m => {
                if (m.senderId === message.id) {
                    subscriber.next(m);
                    subscriber.complete();
                }
            });
        });
    }
}