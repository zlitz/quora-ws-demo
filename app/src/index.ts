import { ChatService } from './data/chat.service';
import { WsService } from './data/ws.service';
import './index.less';

export class Main {

    private chattersEl: HTMLInputElement | undefined;
    private logEl: HTMLInputElement | undefined;
    private textInput: HTMLInputElement | undefined;
    private nameInput: HTMLInputElement | undefined;
    private connectButton: HTMLButtonElement | undefined;
    private readonly _chatService: ChatService;
    private _state: State;

    public constructor() {
        this._chatService = new ChatService(new WsService('ws://localhost:1301'));
        this._state = {
            chatters: {},
            isConnected: false,
            name: '',
            userId: -1
        };

        this.initElements();

        this._chatService.chatterDisconnected().subscribe(m => {
            this._state.chatters = this._state.chatters || {};
            delete this._state.chatters[m.userId];
            this.updateViewFromState();
        })

        this._chatService.chatterConnected().subscribe(m => {
            this._state.chatters = this._state.chatters || {};
            this._state.chatters[m.userId] = m.name;
            this.updateViewFromState();
        })

        this._chatService.messageReceived().subscribe(m => {
            const divEl = document.createElement('div');
            const chattersName = this._state.chatters[m.userId] || 'unknown';
            divEl.appendChild(document.createTextNode(`${chattersName}: ${m.message}`));
            this.logEl!.appendChild(divEl);
        })
    }


    private initElements() {

        this.chattersEl = document.querySelector('#chatters') as HTMLInputElement;
        this.logEl = document.querySelector('#log') as HTMLInputElement;
        this.textInput = document.querySelector('#textInput') as HTMLInputElement;
        this.nameInput = document.querySelector('#nameInput') as HTMLInputElement;
        this.connectButton = document.querySelector('#connectButton') as HTMLButtonElement;

        this.textInput.onkeydown = event => this.onChatMessageChange(event);
        this.nameInput.oninput = () => this.onNameChange(this.nameInput!.value);
        this.connectButton.onclick = () => this.onConnectClick();

        this.updateViewFromState();
    }

    private onNameChange(value: string) {
        this._state.name = value;
        this.updateViewFromState();
    }

    private onChatMessageChange(event: KeyboardEvent): void {
        if (event.key === 'Enter') {
            this._chatService.sendMessage(this.textInput!.value, this._state.userId);
            this.textInput!.value = '';
            this.textInput!.selectionStart = 0;
            this.textInput!.selectionEnd = 0;
        }
    }

    private onConnectClick(): void {
        this._chatService.connect(this._state.name).subscribe(m => {
            this._state.chatters = {};
            this._state.chatters[m.userId] = m.name;
            (m.users || []).forEach(user => this._state.chatters[user.userId] = user.name);
            this._state.isConnected = true;
            this._state.userId = m.userId;
            this.updateViewFromState();
        });
    }

    private updateViewFromState() {
        const names = Object.keys(this._state.chatters || {}).map<string>((id: string) => this._state.chatters[id as any]);
        this.chattersEl!.innerText = names.join('\r\n');
        this.connectButton!.disabled = this._state.isConnected === true || (this._state.name || '').length < 1;
    }
}

type State = {
    chatters: { [id: number]: string };
    isConnected: boolean;
    name: string;
    userId: number;
}

new Main();