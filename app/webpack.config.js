const nPath = require('path');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: './src/index',
    output: {
        path: nPath.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.less$/,
                use: [
                    {
                        loader: 'style-loader' // creates style nodes from JS strings
                    },
                    {
                        loader: 'css-loader' // translates CSS into CommonJS
                    },
                    {
                        loader: 'less-loader' // compiles Less to CSS
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader' // creates style nodes from JS strings
                    },
                    {
                        loader: 'css-loader' // translates CSS into CommonJS
                    }
                ]
            },
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
      extensions: [ '.tsx', '.ts', '.js' ]
    },
    plugins: [
        new CopyPlugin([
            {
                from: 'src/**/*.+(html|png)',
                to: './', transformPath(target, fullPath) {
                    // Remove the src base path
                    const BASE_PATH = 'src';
                    const regex = new RegExp(`^${BASE_PATH}`, 'i');
                    return target.replace(regex, '');
                }
            }
        ])
    ]
}