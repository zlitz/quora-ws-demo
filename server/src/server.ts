import express from 'express';
import * as nPath from 'path';

export class Server {

    private readonly _app: express.Express;
    private readonly _port: number;

    public constructor(port: number) {
        this._port = port;
        this._app = express();
    }

    public start(): void {
        this._app.use('/', express.static(nPath.resolve(__dirname, '../../app/dist')));
        this._app.listen(this._port, () => console.log(`http://localhost:${this._port}`));
    }
}