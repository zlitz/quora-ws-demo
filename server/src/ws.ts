import WebSocket from 'ws';

export class Ws {

    private _ws: WebSocket.Server;
    private _chatters: Map<WebSocket, Chatter> = new Map();
 
    public constructor(port: number) {
        this._ws = new WebSocket.Server({ port: port });
        this._ws.on('connection', ws => {
            ws.on('close', () => {
           
                const chatter = this._chatters.get(ws);
                if (chatter == undefined) {
                    return;
                }

                this._chatters.delete(ws);
                const m = {
                    data: { name: chatter.name, userId: chatter.id },
                    id: Math.random(),
                    senderId: -1,
                    type: 'userLeft'
                };

                for (let item of this._chatters.keys()) {
                    item.send(JSON.stringify(m));
                }
            });
            ws.on('message', (message: any) => {
                const data = JSON.parse(message);
                if (data.type === 'connect') {

                    const userId = Math.random();
                    const joinedMessage = {
                        data: { name: data.data.name, userId: userId },
                        id: Math.random(),
                        senderId: -1,
                        type: 'userJoined'
                    }

                    const chatters: {userId: number, name: string}[] = [];
                    for (let item of this._chatters.entries()) {
                        item[0].send(JSON.stringify(joinedMessage));
                        chatters.push({ name: item[1].name, userId: item[1].id });
                    }

                    this._chatters.set(ws, {
                        id: userId,
                        name: data.data.name
                    });

                    
                    
                    const response = {
                        data: { 
                            name: data.data.name,
                            userId: userId,
                            users: chatters
                        },
                        id: Math.random(),
                        senderId: data.id,
                        type: 'chatConnected'
                    };

                   ws.send(JSON.stringify(response));

                } else if (data.type === 'newMessage') {
                    const response = {
                        data: { userId: data.data.userId, message: data.data.message },
                        id: Math.random(),
                        senderId: data.id,
                        type: 'newMessage'
                    }

                    for (let item of this._chatters.entries()) {
                        item[0].send(JSON.stringify(response));
                    }
                }
            });
        });
    }
}

type Chatter = {
    name: string;
    id: number;
}